import { gql } from '@apollo/client'

export const GET_POSTS_QUERY = gql`
query {
  posts{
    id
    title
    body
    published
    updatedAt
    createdAt
  }
}
`;