import React from 'react';
import { useQuery } from '@apollo/client'

import { GET_POSTS_QUERY } from '../utils/graphql';
import PostCard from '../components/PostCard';

function Home() {
    const { loading, data } = useQuery(GET_POSTS_QUERY);
    return (
        <div className="justify-center pt-12">
            {loading ? (
                <div className="text-center">Loading...</div>
            ) : (
                <div className="justify-center grid grid-cols-1 md:grid-cols-2 2xl:grid-cols-3 gap-8">
                    {data.posts.map(post => (
                        <PostCard post={post} />
                    ))}
                </div>
            )}
        </div>
    );
}

export default Home;

//flex flex-wrap flex-column justify-center