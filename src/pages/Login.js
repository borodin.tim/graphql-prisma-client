import React from 'react';

function Login() {
    const htmlContent = (
        // <div className="justify-center">
        //     <h1 className="text-xl my-4">Login</h1>

        // </div>

        <div>
            <div class="min-h-screen flex justify-center pt-12 py-12 px-4 sm:px-6 lg:px-8">
                <div class="max-w-md w-full">
                    <h2 class="mt-6 text-center text-3xl font-extrabold text-gray-900">
                        Log in to your account
                        </h2>
                    <form class="mt-8 space-y-6" action="#" method="POST">
                        <input type="hidden" name="remember" value="true" />
                        <div class="rounded-md shadow-sm -space-y-px">
                            <div>
                                <label for="email-address" class="sr-only">Email address</label>
                                <input id="email-address" name="email" type="email" autocomplete="email" required class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-green-500 focus:border-green-500 focus:z-10 sm:text-sm" placeholder="Email address" />
                            </div>
                            <div>
                                <label for="password" class="sr-only">Password</label>
                                <input id="password" name="password" type="password" autocomplete="current-password" required class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-green-500 focus:border-green-500 focus:z-10 sm:text-sm" placeholder="Password" />
                            </div>
                        </div>

                        <div class="flex items-center justify-between">
                            <div class="flex items-center">
                                <input disabled id="remember_me" name="remember_me" type="checkbox" class="h-4 w-4 text-green-600 focus:ring-green-500 border-gray-300 rounded" />
                                <label for="remember_me" class="ml-2 block text-sm text-gray-900">
                                    Remember me
                                </label>
                            </div>

                            <div class="text-sm">
                                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                                    Forgot your password?
                                </a>
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
                                <span class="absolute left-0 inset-y-0 flex items-center pl-3">
                                    <svg class="h-5 w-5 text-green-500 group-hover:text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                                Log in
                    </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );

    return htmlContent;
}

export default Login;

// <div className="flex flex-col bg-yellow-100 max-w-1xl">
//                 <form>
//                     <input
//                         className="rounded border"
//                         label="Username"
//                         placeholder="Username"
//                         name="username"
//                         type="text"
//                         focus
//                     />
//                     <input
//                         className="rounded border"
//                         label="Password"
//                         placeholder="Password"
//                         name="password"
//                         type="password"
//                     />
//                     <button
//                         className="rounded border px-4 py-2 bg-white"
//                         type="Submit"
//                         primary
//                     >
//                         Login
//                 </button>
//                 </form>
//             </div>