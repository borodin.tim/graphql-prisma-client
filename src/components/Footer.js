import React from 'react';

function Footer() {
    const footer = (
        <div className="text-center w-full py-4">
            <small>Created by Tim Borodin 2021</small>
        </div>
    );

    return footer;
}

export default Footer;