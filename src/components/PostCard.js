import React from 'react';

function PostCard({ post }) {
    const content = (
        <div className="border-black max-w-md w-full" key={post.id}>
            <div className="bg-green-800 py-2 px-4 text-white">{post.title}</div>
            <div className="bg-green-400 py-2 px-4 text-black text-sm">{post.body}</div>
        </div>
    );

    return content;
}

export default PostCard;
