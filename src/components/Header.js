import React from 'react';
import { Link } from 'react-router-dom';

import MenuBar from './MenuBar'

function Header() {
    const header = (
        <div className="bg-green-100 px-4 py-4 items-centerw-screen">
            <div className="flex flex-row mx-auto xl:max-w-screen-xl md:max-w-screen-md justify-between">
                <h1>
                    <Link className="text-3xl font-extrabold" to="/">Blogging App</Link>
                </h1>
                <MenuBar />
            </div>
        </div>
    );

    return header;
}

export default Header;

// container