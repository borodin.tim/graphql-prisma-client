import React from 'react';
import { NavLink } from 'react-router-dom';

function MenuBar() {
    const menuBar = (
        <div className="flex flex-row">
            <NavLink className="pr-5 text-xl m-auto" to="/" title="Home" exact>
                Home
            </NavLink>
            <NavLink className="pr-5 text-xl m-auto" to="/login" title="Login">
                Login
            </NavLink>
            <NavLink className="pr-5 text-xl m-auto" to="/register" title="Register">
                Register
            </NavLink>
        </div>
    );

    return menuBar;
}

export default MenuBar;

//activeClassName="font-black"