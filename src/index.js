import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import ApolloProvider from './ApolloProvider';
import './tailwind.output.css';

ReactDOM.render(ApolloProvider, document.getElementById('root'));

reportWebVitals();
